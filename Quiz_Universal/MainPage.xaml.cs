﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Quiz_Universal
{

    class CaseClass
    {
        public int Result { get; set; }
        public int Time { get; set; }
        public string Login { get; set; }
    }

        public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            user_ans.IsEnabled = false;
            generate_question.IsEnabled = false;
            answer_btn.IsEnabled = false;
        }

        DispatcherTimer timer1;
        DispatcherTimer timer3;
        int timeLeft, allTime;
        int points = 0;
        string login;


        public delegate void EventHandler(object sender, EventArgs e);

        public void DispatcherTimerSetup()
        {
            timer1 = new DispatcherTimer();
            timer1.Tick += timer1_Tick;
            timer1.Interval = new TimeSpan(0, 0, 1);
            timer1.Start();
        }



        private void message_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }



        private async void start_btn_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(username.Text))
            {
                login = username.Text;
                username.Visibility = Visibility.Collapsed;
                loginLabel.Text = login;
                user_ans.IsEnabled = true;
                answer_btn.IsEnabled = true;
                generate_question.IsEnabled = true;
                Random rand = new Random();
                operand1.Text = rand.Next(0, 30).ToString();
                operand2.Text = rand.Next(0, 30).ToString();
                punktacja.Text = "0";
                start_btn.Visibility = Visibility.Collapsed;
                finish.Visibility = Visibility.Visible;
                timeLeft = 10;
                allTime = 0;
                timeLabel.Text = "10 seconds";
                DispatcherTimerSetup();
                timer3 = new DispatcherTimer();
                timer3.Tick += timer3_Tick;
                timer3.Interval = new TimeSpan(0, 0, 1);
                timer3.Start();
            }
            else
            {
                MessageDialog messageDialog1 = new MessageDialog("Please write your username", "Sorry!");
                await messageDialog1.ShowAsync();
            }

        }

        private async void timer3_Tick(object sender, object e)
        {

            allTime += 1;
            timeLeft = timeLeft - 1;
            AlltimeLabel.Text = allTime + " seconds";

        }


        private async void timer1_Tick(object sender, object e)

        {
            {
                if (timeLeft > 0)
                {
                    timeLeft = timeLeft - 1;
                    timeLabel.Text = timeLeft + " seconds";
                }
                else
                {
                    timer1.Stop();
                    timeLabel.Text = "Time's up!";
                    MessageDialog messageDialog = new MessageDialog("You didn't finish in time.", "Sorry!");
                    await messageDialog.ShowAsync();
                    start_btn.IsEnabled = true;
                    message.Text = "Your answer is incorrect!";
                    user_ans.IsEnabled = false;
                    generate_question.IsEnabled = true;
                    answer_btn.IsEnabled = false;
                }

            }
        }

        private void answer_btn_Click(object sender, RoutedEventArgs e)
        {
            if (int.Parse(operand1.Text) + int.Parse(operand2.Text) == int.Parse(user_ans.Text))
            {
                points += 1;
                punktacja.Text = points.ToString();
                message.Text = "Your answer is correct!";
                timer1.Stop();

            }
            else
            {
                message.Text = "Your answer is incorrect!";
                timer1.Stop();
            }
            user_ans.IsEnabled = false;
            generate_question.IsEnabled = true;
            answer_btn.IsEnabled = false;
        }

        private void generate_question_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            Random rand = new Random();
            operand1.Text = rand.Next(0, 30).ToString();
            operand2.Text = rand.Next(0, 30).ToString();

            user_ans.IsEnabled = true;
            generate_question.IsEnabled = true;
            answer_btn.IsEnabled = true;

            user_ans.Text = "";
            message.Text = "";

            timeLeft = 10;
            timeLabel.Text = "10 seconds";
            DispatcherTimerSetup();
        }

        private async void finish_Click(object sender, RoutedEventArgs e)
        {
            timer3.Stop();
            timer1.Stop();
            user_ans.IsEnabled = false;
            generate_question.IsEnabled = false;
            answer_btn.IsEnabled = false;
            start_btn.Visibility = Visibility.Visible;
            username.Visibility = Visibility.Visible;
            loginLabel.Visibility = Visibility.Collapsed;
            finish.Visibility = Visibility.Collapsed;
            message.Text = " ";


            try
            {

                Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile2 =
                await storageFolder.GetFileAsync("sample.txt");
                await Windows.Storage.FileIO.AppendTextAsync(sampleFile2, login + ";" + allTime + ";" + points + "\r\n");

            }
            catch (Exception exc) { message.Text = exc.Message; }

        }

        private async void ranking_btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<CaseClass> users = new List<CaseClass>();
                Windows.Storage.StorageFolder storageFolder =
                Windows.Storage.ApplicationData.Current.LocalFolder;
                Windows.Storage.StorageFile sampleFile =
                await storageFolder.GetFileAsync("sample.txt");

                var lines = await Windows.Storage.FileIO.ReadLinesAsync(sampleFile);
       
                var pom = lines.Select(a => a.Split(';'));
                var cvs = pom.ToArray();
                for (int i = 0; i < cvs.Length; i++)
                {
                    users.Add(new CaseClass { Login = cvs[i][0], Time = Convert.ToInt32(cvs[i][1]), Result=Convert.ToInt32( cvs[i][2] )});
                }
      
                var query = from casee in users
                            orderby casee.Result descending 
                            select casee;

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Lp."+" "+"User"+ " " + "Time" + " " + "Result");
                int j = 1;
                foreach (var userr in query)
                {
                    // message.Text = userr.Login.ToString()+" "+userr.Time.ToString()+" "+userr.Result.ToString();
                    sb.AppendLine(j.ToString()+"      "+userr.Login.ToString() + "  " + userr.Time.ToString() + " " + userr.Result.ToString());
                    j++;
                }
               // MessageBox.Show(sb.ToString());

                MessageDialog messageDialog1 = new MessageDialog(sb.ToString(), "Ranking");
                await messageDialog1.ShowAsync();
            }
            catch (Exception exc) { message.Text = exc.Message; }
        }

        private void textBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }
    }
}
